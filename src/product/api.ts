import axios from "axios";

import {Product} from "./types";

const requester = axios.create({
  baseURL: "https://coding-challenge-api.aerolab.co",
  headers: {
    Authorization: `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI2MDI5NjIwZjdlNzE4NzAwMjBlMzhlZjUiLCJpYXQiOjE2MTMzMjQ4MTV9.YwxmvA6s0eW-IbuoRzme6xLfaIALy3RhSRiR3GrMzwo`,
  },
});

requester.interceptors.response.use((response) => response.data);
export default {
  list: (): Promise<Product[]> => requester.get(`/products`, {}),
  // list: (): Promise<Product[]> =>
  //   Promise.resolve([
  //     {
  //       _id: "5a033eeb364bf301523e9b92",
  //       name: "Sandalia Pale Gold YSL",
  //       cost: 200,
  //       category: "Indumentaria",
  //       img: {
  //         url: "https://coding-challenge-api.aerolab.co/images/Alienware13-x2.png",
  //         hdUrl:
  //           "https://coding-challenge-api.aerolab.co/images/Alienware13-x2.png",
  //       },
  //     },
    //   {
    //     _id: "5a033f0f364bf301523e9b93",
    //     name: "iPhone 7 Case Sea-Blue",
    //     cost: 200,
    //     category: "Accesorios",
    //     img: {
    //       url: "https://coding-challenge-api.aerolab.co/images/SamsungTabS2-x1.png",
    //       hdUrl:
    //         "https://coding-challenge-api.aerolab.co/images/SamsungTabS2-x1.png",
    //     },
    //   },
    // ]),
    redeem: (product: Product): Promise<string> =>
      Promise.resolve(`You have redeem the product successfully (${product.name})`)
};
